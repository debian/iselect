// SPDX-License-Identifier: 0BSD


extern "C" {
#include "enter.h"
}

#include <algorithm>
#include <bit>
#include <ctype.h>
#include <limits.h>
#include <numeric>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>


struct character {
	wchar_t c;
	char seg[MB_LEN_MAX];
	unsigned seglen : std::bit_width(static_cast<unsigned>(MB_LEN_MAX));
	unsigned width : 2;
	bool space : 1;
	bool word : 1;
};
static_assert(MB_LEN_MAX <= ((1 << std::bit_width(static_cast<unsigned>(MB_LEN_MAX))) - 1));


extern "C" bool enter_string(WINDOW * win, FILE * out, bool emptyok) {
	int y, x;
	getyx(win, y, x);
	km_dokey_load_termios();

#define MEASURE(character)                 \
	mvwaddnwstr(win, y, x, &character.c, 1); \
	character.width = getcurx(win) - x;      \
	character.space = iswspace(character.c); \
	character.word  = iswalnum(character.c)

	struct character * characters{};
	size_t characters_len{}, characters_cap{};
#define BUMPCHAR()                                                                                                            \
	do {                                                                                                                        \
		if(characters_len == characters_cap) {                                                                                    \
			characters_cap = characters_cap ? characters_cap * 2 : 16;                                                              \
			if(!(characters = reinterpret_cast<struct character *>(reallocarray(characters, characters_cap, sizeof(*characters))))) \
				return false;                                                                                                         \
		}                                                                                                                         \
	} while(false)

	size_t characters_cursor = characters_len;


#define LEFTBY(howmuch)                                \
	do {                                                 \
		int moveleft = howmuch;                            \
		while(first_on_screen && moveleft > 0)             \
			moveleft -= characters[--first_on_screen].width; \
	} while(false)
#define RIGHTBY(howmuch)                                      \
	do {                                                        \
		int moveright = howmuch;                                  \
		while(first_on_screen != characters_len && moveright > 0) \
			moveright -= characters[++first_on_screen].width;       \
	} while(false)
	// Write out from first_on_screen, so long as the cursor fits on the screen, else move by a half-screen to find it.
	// Start off with as much text as possible and cursor on the right margin; you can't return to this state; this mimicks how ≤1b did it
	size_t first_on_screen = characters_cursor;
	LEFTBY((getmaxx(win) - x) - 1);

	for(wint_t input;;) {
		{
			auto screen_width         = getmaxx(win) - x;
			auto all_characters_width = std::accumulate(characters, characters + characters_len, 0z, [](auto acc, auto && c) { return acc + c.width; });
			if(all_characters_width < screen_width)
				first_on_screen = 0;
			else {
				if(first_on_screen >= characters_len) {
					first_on_screen = characters_cursor - 1;
					LEFTBY(screen_width / 2);
				} else if(characters_cursor < first_on_screen)
					while(characters_cursor < first_on_screen)
						LEFTBY(screen_width / 2);
				else
					for(;;) {
						auto last = std::find_if(characters + first_on_screen, characters + characters_len, [&, acc = 0z](auto && c) mutable {
							acc += c.width;
							return acc > screen_width;
						});

						auto characters_on_screen = last - (characters + first_on_screen);
						if(first_on_screen + characters_on_screen < characters_cursor)
							RIGHTBY(screen_width / 2);
						else
							break;
					}
			}

			wmove(win, y, x);
			auto cursor_x = x;
			for(auto itr = characters + first_on_screen; itr != characters + characters_len; ++itr) {
				if(itr->width <= screen_width)
					screen_width -= itr->width;
				else
					break;
				waddnwstr(win, &itr->c, 1);
				if(itr < characters + characters_cursor)
					cursor_x += itr->width;
			}
			wclrtoeol(win);
			wmove(win, y, cursor_x);
		}

		switch(km_dokey(win, &input)) {
			case -1:
				return free(characters), false;

			case OP_EDITOR_CHAR:
				if(iswprint(input)) {
					struct character character = {};
					character.c                = input;

					int newlen = wctomb(character.seg, input);
					if(newlen == -1)
						goto err;
					character.seglen = newlen;

					MEASURE(character);

					BUMPCHAR();
					memmove(characters + characters_cursor + 1, characters + characters_cursor, (characters_len - characters_cursor) * sizeof(*characters));
					++characters_len;
					characters[characters_cursor++] = character;
				} else {
				err:
					flushinp();
					beep();
				}
				break;

			case OP_EDITOR_DONE: {
				if(!characters_len && !emptyok) {
					wmove(win, y, x);
					break;
				}

				for(auto itr = characters; itr != characters + characters_len; ++itr)
					fwrite(itr->seg, 1, itr->seglen, out);
				return free(characters), true;
			} break;

			case OP_EDITOR_RESIZE:
				break;

			case OP_EDITOR_IDK:
				beep();
				break;

			case OP_EDITOR_BACKSPACE:
				if(!characters_cursor)
					beep();
				else {
					memmove(characters + characters_cursor - 1, characters + characters_cursor, (characters_len - characters_cursor) * sizeof(*characters));
					--characters_len;
					--characters_cursor;
				}
				break;
			case OP_EDITOR_DELETE_CHAR:
				if(characters_cursor == characters_len)
					beep();
				else {
					memmove(characters + characters_cursor, characters + characters_cursor + 1, (characters_len - (characters_cursor + 1)) * sizeof(*characters));
					--characters_len;
				}
				break;
			case OP_EDITOR_KILL_LINE:
				characters_len = characters_cursor = 0;
				break;
			case OP_EDITOR_KILL_EOL:
				characters_len = characters_cursor;
				break;
			case OP_EDITOR_KILL_WORD: {
				if(!characters_cursor)
					break;

				auto end = characters_cursor;
				--characters_cursor;
				while(characters_cursor && characters[characters_cursor - 1].space)
					--characters_cursor;
				while(characters_cursor && characters[characters_cursor - 1].word)
					--characters_cursor;

				memmove(characters + characters_cursor, characters + end, (characters_len - end) * sizeof(*characters));
				characters_len -= end - characters_cursor;
			} break;

			case OP_EDITOR_BOL:
				characters_cursor = 0;
				break;
			case OP_EDITOR_EOL:
				characters_cursor = characters_len;
				break;
			case OP_EDITOR_BACKWARD_CHAR:
				if(!characters_cursor)
					beep();
				else
					--characters_cursor;
				break;
			case OP_EDITOR_FORWARD_CHAR:
				if(characters_cursor == characters_len)
					beep();
				else
					++characters_cursor;
				break;
		}
	}
}
