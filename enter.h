// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright (C) 1997 Michael R. Elkins <me@cs.hmc.edu>


#include <curses.h>
#include <stdbool.h>


enum {
	OP_EDITOR_CHAR,
	OP_EDITOR_DONE,
	OP_EDITOR_RESIZE,
	OP_EDITOR_IDK,

	OP_EDITOR_BACKSPACE,
	OP_EDITOR_DELETE_CHAR,
	OP_EDITOR_KILL_LINE,
	OP_EDITOR_KILL_EOL,
	OP_EDITOR_KILL_WORD,

	OP_EDITOR_BOL,
	OP_EDITOR_EOL,
	OP_EDITOR_BACKWARD_CHAR,
	OP_EDITOR_FORWARD_CHAR,
};

extern bool enter_string(WINDOW * win, FILE * out, bool emptyok);


extern void km_dokey_load_termios(void);
extern int km_dokey(WINDOW * win, wint_t * LastKey);
