# SPDX-License-Identifier: GPL-2.0-only
  <b>    _ ____       _           _
  <b>   (_) ___|  ___| | ___  ___| |_
  <b>  / /\___ \ / _ \ |/ _ \/ __| __|
  <b> / /  ___) |  __/ |  __/ (__| |_
  <b>(_(  |____/ \___|_|\___|\___|\__|

  <b>iSelect -- Interactive Selection Tool

  Cursor Movement:
      <b>CURSOR-UP</b> ..... Move cursor one line up
      <b>CURSOR-DOWN</b> ... Move cursor one line down
      <b>PAGE-UP</b> ....... Move cursor one page up
      <b>PAGE-DOWN</b> ..... Move cursor one page down
      <b>g</b> ............. Goto first line
      <b>G</b> ............. Goto last line

  Line Selection:
      <b>RETURN</b> ........ Select line and exit
      <b>CURSOR-RIGHT</b> .. Select line and exit
      <b>SPACE</b> ......... Select line and stay (multi-line mode only)
      <b>C</b> ............. Clear current marks  (multi-line mode only)

  Others:
      <b>q</b> ............. Quit (exit without selection)
      <b>CURSOR-LEFT</b> ... Quit (exit without selection)
      <b>h</b> ............. Help Page (this page)
      <b>v</b> ............. Version Page
