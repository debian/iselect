# SPDX-License-Identifier: GPL-2.0-only

l () {
    cmd="$(ilogin)";
    if [ -n "$cmd" ]; then
        printf '$ %s\n' "$cmd"
        eval "$cmd"
    fi
}
