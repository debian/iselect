##
##  .bashrc snippet for iSelect-based 'cd' command
##  Copyright (c) 1997 Ralf S. Engelschall, All Rights Reserved.
##

#   database scan
cds () {
    find "$HOME" -type d | sort > ~/.dirs &
}

#   enhanced cd command
cd () {
    if [ -d "$1" ]; then
        builtin cd "$1"
    else
        builtin cd "$(grep -E "/$1[^/]*$" ~/.dirs   |
                      iselect -a -Q "$1" -n "chdir" \
                      -t "Change Directory to...")"
    fi
}

#   change to parent dir
alias -- -='cd ..'

#   change to last dir
alias .='cd "$OLDPWD"'
