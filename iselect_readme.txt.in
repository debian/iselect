# SPDX-License-Identifier: GPL-2.0-only
  <b>    _ ____       _           _
  <b>   (_) ___|  ___| | ___  ___| |_
  <b>  / /\___ \ / _ \ |/ _ \/ __| __|
  <b> / /  ___) |  __/ |  __/ (__| |_
  <b>(_(  |____/ \___|_|\___|\___|\__|

  <b>iSelect -- Interactive Selection Tool

  iSelect is an interactive line selection tool,
  operating via a full-screen Curses-based terminal session.
  It can be used either as a user interface frontend controlled by
  a shell/Perl/Tcl backend as its control script, or in batch mode as
  a pipe filter (usually between grep and the final executing command).

  Version @ISELECT_VERSION_LONG@

  This is a thawed OSSP project; for git repository/bug tracker/mailing list see
    https://sr.ht/~nabijaczleweli/ossp

  The manual is available on-line and at
    https://srhtcdn.githack.com/~nabijaczleweli/ossp-iselect/blob/man/ossp-iselect.pdf

  The latest release can be found on
  http://www.ossp.org/pkg/tool/iselect/

  Copyright (c) 1997-2007 Ralf S. Engelschall <rse@engelschall.com>

  This program is free software; it may be redistributed and/or modified
  only under the terms of the GNU General Public License, which may be found
  in the iSelect source distribution.  Look at the file LICENSES/GPL-2.0-only.txt for details.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the the GNU General Public
  License for more details.

                              Ralf S. Engelschall
                              rse@engelschall.com
                              www.engelschall.com
