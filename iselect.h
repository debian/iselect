// SPDX-License-Identifier: GPL-2.0-only
// Copyright (c) 1997-2007 Ralf S. Engelschall.

#include <stdbool.h>
#include <curses.h>
#include <sys/types.h>

enum iSelect_key {
    iSelect_DontKnow = 0xD800,  // surrogate, not a valid character
    iSelect_NextLine,
    iSelect_PrevLine,
    iSelect_NextPage,
    iSelect_PrevPage,
    iSelect_Left,   // these are for display only, we force-map them to q and enter internally
    iSelect_Right,
};
_Static_assert(sizeof(enum iSelect_key) >= sizeof(wint_t), "enum iSelect_key : wint_t");

extern void configure_custom_key(char *config);
extern enum iSelect_key mapped_key(WINDOW *win, enum iSelect_key *unmapped_key);
extern void name_key(enum iSelect_key key, FILE *into);


/*
 *  The Structure of our screen lines
 */
typedef struct Line {
    char *cpLine;           /* the input line */
    size_t nLine;           /* input line length */
    char *cpResult;         /* the result string */
    size_t nResult;         /* result string length */
    char *cpFmtResult;      /* the formatted result string (use instead of cpResult if set) */
    size_t nFmtResult;      /* formatted result string length */
    bool   fSelectable : 1; /* whether selectable or not */
    bool   fSelected : 1;   /* whether already selected or not */
} Line;

extern Line *spaLines;
extern size_t  nLines;

extern ssize_t iSelect(size_t pos, const char *title, const char *name,
                   const char *tagbegin, const char *tagend, bool stripws,
                   bool browsealways, bool allselectable,
                   bool multiselect, bool exitnoselect,
                   enum iSelect_key *unmapped_key);


extern const Line iSelect_Help[];
extern const Line iSelect_README[];
